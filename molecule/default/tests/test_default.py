import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_container(host):
    with host.sudo():
        mssql = host.docker("docker-mssql")
        assert mssql.is_running
